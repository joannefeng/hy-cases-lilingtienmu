exports.info = {
    title: "力麒天沐",
    description: "天母核心美境，磺溪第一排、綠堤樹海之上，俯瞰公園林蔭、磺溪彩虹橋相伴，生活綠景一氣呵成；力麒建設、李天鐸大師聯手共塑藝術建築，得天獨厚、一遇難求。",
    ogTitle: "力麒天沐 天母磺溪畔、森情之首‧李天鐸‧92-98坪SC鋼骨",
    ogDescription: "天母核心美境，磺溪第一排、綠堤樹海之上，俯瞰公園林蔭、磺溪彩虹橋相伴，生活綠景一氣呵成；力麒建設、李天鐸大師聯手共塑藝術建築，得天獨厚、一遇難求。",
    ogImage: "https://case.hiyes.tw/lilingtienmu/img/og.jpg",
    keywords: "力麒天沐"
};